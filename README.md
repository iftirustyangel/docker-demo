# Simple CRUD-Web (REDIS and Flask) deployed on docker as container
This is a simple example of Python Flask's using Redis as Database Management System. 

### Installation

run this following command:

```
$ sudo pip install -r requirements.txt
```

It will install Flask, Flask-WTF, and Redis.

### Usage

To run the program, first you should run the redis-server:

```
$ redis-server
```

then, run the program:

```
$ python run.py
```

Open your browser and go to `localhost:5000	` to see the running program.

### Docker:
## Only Web App as contianer

1- Make sure you have redis running locally
2-  
```
$ docker build  webapp .
```
```
$ docker run -p 80:5000 webapp:latest
```

## Web App and redis as containerg as service

```
$ pip install docker-compose
```

```
$ docker-compose up --build
```

3-  hit localhost on your browser

