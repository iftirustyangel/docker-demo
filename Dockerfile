FROM python:3.6.8-alpine3.9
RUN pip install virtualenv
WORKDIR /home
COPY . .
RUN virtualenv myenv && source myenv/bin/activate
RUN pip install -r requirements.txt
RUN chmod 777 app.py
ENTRYPOINT ["python"]
CMD ["app.py"]