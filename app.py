from flask import Flask, render_template, request, redirect, url_for
from flask_wtf import FlaskForm
from wtforms import TextField, IntegerField, SubmitField
import redis
import json
import time

# config system
app = Flask(__name__)
app.config.update(dict(SECRET_KEY='yoursecretkey'))
r = redis.Redis(host='redis', port=6379)

# set auto-generated key
try:
    if(r.exists('id') == False):
        r.set('id', '0')
except Exception as ex:
    print(ex)
    pass


class CreateTask(FlaskForm):
    title = TextField('Task Title')
    shortdesc = TextField('Short Description')
    priority = IntegerField('Priority')
    create = SubmitField('Create')


class DeleteTask(FlaskForm):
    key = TextField('Task Key')
    title = TextField('Task Title')
    delete = SubmitField('Delete')


class UpdateTask(FlaskForm):
    key = TextField('Task Key')
    title = TextField('Task Title')
    shortdesc = TextField('Short Description')
    priority = IntegerField('Priority')
    update = SubmitField('Update')


class ResetTask(FlaskForm):
    reset = SubmitField('Reset')


def createTask(form):
    title = form.title.data
    priority = form.priority.data
    shortdesc = form.shortdesc.data
    task = {'title': title, 'shortdesc': shortdesc, 'priority': priority}

    # set auto-generated key
    r.set(str(r.get('id')), json.dumps(task))
    r.incr('id')
    return redirect('/')


def deleteTask(form):
    key = form.key.data
    title = form.title.data
    if(key):
        r.delete(key)
    else:
        for i in r.keys():
            if i != 'id' and r.get(i):
                print(i)
                r.delete(i)
    return redirect('/')


def updateTask(form):
    title = form.title.data
    priority = form.priority.data
    key = form.key.data
    shortdesc = form.shortdesc.data
    task = {'title': title, 'shortdesc': shortdesc, 'priority': priority}

    # update by "reset"
    if(r.exists(key)):
        r.set(key, json.dumps(task))
    return redirect('/')


def resetTask(form):
    r.flushall()
    r.set('id', '0')
    return redirect('/')


@app.route('/', methods=['GET', 'POST'])
def main():
    # create form
    cform = CreateTask(prefix='cform')
    dform = DeleteTask(prefix='dform')
    uform = UpdateTask(prefix='uform')
    reset = ResetTask(prefix='reset')

    # response
    if cform.validate_on_submit() and cform.create.data:
        return createTask(cform)
    if dform.validate_on_submit() and dform.delete.data:
        return deleteTask(dform)
    if uform.validate_on_submit() and uform.update.data:
        return updateTask(uform)
    if reset.validate_on_submit() and reset.reset.data:
        return resetTask(reset)

    # get all data
    keys = []
    val = {}
    try:
        keys = r.keys()
        for i in keys:
            if i != 'id':
                val[i] = json.loads(r.get(i))
    except Exception as ex:
        print(ex)
        pass

    return render_template('index.html', cform=cform, dform=dform, uform=uform,
                           keys=keys, val=val, reset=reset)


if __name__ == '__main__':
    app.run(host="0.0.0.0")
